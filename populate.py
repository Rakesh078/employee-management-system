from EmployeeManagement.models import Employee, Department, Desination
from datetime import datetime

d1 = Department(depid = 1 , name_dept = 'Technical', indentend_strength= 20)
d1.save()

d2 = Department(depid = 2 ,name_dept = 'Marketing', indentend_strength = 20)
d2.save()

d3 = Department(depid = 3 ,name_dept = 'Account', indentend_strength = 30)
d3.save()

de1 = Desination(desination_name = 'Software Engineer',strength =  55 )
de1.save()

de2 = Desination(desination_name = 'Sales Manager',strength = 25 )
de2.save()

de3 = Desination(desination_name = 'Account Manager',strength = 25)
de3.save()
de4 = Desination(desination_name = 'Chief Technical Officer',strength =5)
de4.save()

de5 = Desination(desination_name = 'HOD',strength=5 )
de5.save()

Employee(empid = '1', name = 'Sanjeev', dob = '1992-07-13', department = d1, desination = de5, doj = '2016-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '2', name = 'Rajeev', dob = '1992-07-10', department = d2, desination = de5, doj = '2015-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '3', name = 'Abhay', dob = '1991-07-13', department = d3, desination = de5, doj = '2016-08-13',last_working_day = datetime.now().date()).save()
Employee(empid = '4', name = 'Ankit', dob = '1994-07-13', department = d1, desination = de4, doj = '2010-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '5', name = 'Rakesh', dob = '1998-07-18', department = d2, desination = de2, doj = '2011-08-15',last_working_day = datetime.now().date()).save()
Employee(empid = '6', name = 'Sachin', dob = '1999-07-15', department = d3, desination = de3, doj = '2012-09-11',last_working_day = datetime.now().date()).save()
Employee(empid = '7', name = 'Anil', dob = '1990-09-12', department = d1, desination = de1, doj = '2015-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '8', name = 'Aditya', dob = '1997-09-13', department = d1, desination = de1, doj = '2009-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '9', name = 'Deepak', dob = '1991-10-11', department = d1, desination = de1, doj = '2013-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '10', name = 'xyz', dob = '1996-07-13', department = d2, desination = de2, doj = '2014-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '11', name = 'efg', dob = '1995-07-13', department = d1, desination = de1, doj = '2011-07-13',last_working_day = datetime.now().date()).save()
Employee(empid = '12', name = 'ghg', dob = '1992-06-13', department = d2, desination = de2, doj = '2001-05-12',last_working_day = datetime.now().date()).save()
Employee(empid = '13', name = 'jjk', dob = '1990-09-10', department = d2, desination = de2, doj = '2007-07-19',last_working_day = datetime.now().date()).save()
