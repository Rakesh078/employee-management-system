
from django.db import models
#from EmployeeManagement import myField
#import myField

class Employee(models.Model):
    empid = models.IntegerField(primary_key=True)
    name  = models.CharField(max_length=55)
    dob   = models.DateField()
    department= models.ForeignKey('Department')
    desination = models.ForeignKey('Desination')
    doj  = models.DateField()
    last_working_day = models.DateField()
    #def __unicode__(self):
        #return int(empid)


class Department(models.Model):
    depid= models.IntegerField(primary_key=True)
    name_dept = models.CharField(max_length=55)
    indentend_strength = models.IntegerField(default=0)

    #def __unicode__(self):
        #return int(depid)


class Desination(models.Model):
    desination_name = models.CharField(max_length=55)
    strength = models.IntegerField(default=0)

    #def __unicode__(self):
        #return str(desination_name)
