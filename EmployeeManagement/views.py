from django.shortcuts import render
from django.views.generic import View
from django.http import QueryDict
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.

from django.http import HttpResponse
from .models import Employee , Desination, Department
from django.shortcuts import get_object_or_404


# Create your views here.
''' This class dispaly emploee Details from Emploee Databases '''

class EmpInfo(View):
    def get(self, request):
        result = []
        totalemp = Employee.objects.all()
        #length = len(totalemp)
        #print stud
        for emp_info in totalemp:
            dict_res = {}
            dict_res['emp_id'] = emp_info.empid
            dict_res['name'] = emp_info.name
            dict_res['dob'] = emp_info.dob
            dict_res['dept'] = emp_info.department.name_dept
            dict_res['desination'] = emp_info.desination.desination_name
            dict_res['doj'] = emp_info.doj
            dict_res['last_working_day'] = emp_info.last_working_day
            result.append(dict_res)


        return HttpResponse(result, status=400)
'''This Class display information of employee using Department id where department related
    to emplyee table using foreign key '''
class EmployeeOfDep(View):
    def get(self,request,id):
        re = []
        #depart = Department.objects.get(pk=id)
        depart = get_object_or_404(Department, pk=id)
        #print depart
        depts = depart.name_dept
        empinfo = Employee.objects.filter(department__name_dept=depts).values()
        l=len(empinfo)
        dict_res = {}
        for i in range(0,l):

            dict_res['empid'] = empinfo[i]['empid']
            dict_res['name'] = empinfo[i]['name']
            dict_res['dob']= empinfo[i]['dob']
            dict_res['doj']= empinfo[i]['doj']
            dict_res['last_working_day']=empinfo[i]['last_working_day']
            re.append(dict_res)

        return HttpResponse(re, status=400)
'''this class displays number of vacanices on diffent departments '''

class NoVacancies(View):
    def get(self,request):
        totaldep = Department.objects.all().values()
        l=len(totaldep)
        re = []
        dict_res = {}
        for i in range(0, l):
            dict_res['name_dept'] = totaldep[i]['name_dept']
            y =  totaldep[i]['indentend_strength']
            empinfo = Employee.objects.filter(department__name_dept=dict_res['name_dept'])
            total_count = y - len(empinfo)
            if total_count < 0:
                total_count=0
            dict_res['total_count']=total_count
            re.append(dict_res)

        return HttpResponse(re , status=200)
''' This class displays employee information with their desination on compnay '''
class EmpBYDesination(View):

    def get(self,request,desi):
        re = []
        empinfo = Employee.objects.filter(desination__desination_name=desi).values()
     '''
        for emp in empinfo:
            name =  emp.name
            doj =  emp.Doj
            outing_visitor_id = name + str(doj)
    '''

        dict_res={}
        l = len(empinfo)
        for i in range(0, l):
            dict_res['emp_id']=empinfo[i]['empid']
            dict_res['name']=empinfo[i]['name']
            dict_res['dob']=empinfo[i]['dob']
            dict_res['doj']= empinfo[i]['doj']
            dict_res['last_working_day']=empinfo[i]['last_working_day']
            re.append(dict_res)

        return HttpResponse(re, status=400)
