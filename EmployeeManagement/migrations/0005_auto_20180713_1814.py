# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EmployeeManagement', '0004_auto_20180713_0855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='empid',
            field=models.IntegerField(serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='last_working_day',
            field=models.DateField(),
        ),
    ]
