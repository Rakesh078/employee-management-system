# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import EmployeeManagement.myField


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Department',
            fields=[
                ('depid', models.IntegerField(serialize=False, primary_key=True)),
                ('name_dept', models.CharField(max_length=55)),
                ('indentend_strength', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Desination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('desination_name', models.CharField(max_length=55)),
                ('strength', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('empid', models.IntegerField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=55)),
                ('dob', models.DateField()),
                ('doj', models.DateField()),
                ('last_working_day', EmployeeManagement.myField.DayOfTheWeekField(max_length=1, choices=[(b'1', 'Monday'), (b'2', 'Tuesday'), (b'3', 'Wednesday'), (b'4', 'Thursday'), (b'5', 'Friday'), (b'6', 'Saturday'), (b'7', 'Sunday')])),
                ('dept', models.ForeignKey(to='EmployeeManagement.Department')),
                ('desination', models.ForeignKey(to='EmployeeManagement.Desination')),
            ],
        ),
    ]
