# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EmployeeManagement', '0002_auto_20180713_0619'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='depid',
            field=models.IntegerField(serialize=False, primary_key=True),
        ),
    ]
