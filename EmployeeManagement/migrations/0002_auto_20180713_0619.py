# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EmployeeManagement', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='department',
            name='depid',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='employee',
            name='empid',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
    ]
