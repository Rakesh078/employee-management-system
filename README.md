# Employee Management System

This projects contains Employee ,Department and Desination Databases in which Employee information
employee of particular department , total number of vacancies in each departments , employee details
by deginations are displayed . these are the APIS used in our projects

## Running the tests

First clone it using git clone https://Rakesh078@bitbucket.org/Rakesh078/employee-management-system.git in your local machine
and install virtual env , python , django

I have included populated data for data bases , populate.py , migrate the database initially and
open the shell using commands ./manage.py shell and import populate  it will fill databases using populated data .


1. http://127.0.0.1:8000/employee_info/  type this utls on browsers and it will display all details of employee in database.

2. http://127.0.0.1:8000/employeebydesination/Software%20Engineer/ it will diplays employee details according to their desinations .

3. http://127.0.0.1:8000/employeebydepartment/2/  it will diplay employee details at particular departments.

4. http://127.0.0.1:8000/no_of_vacancies_for_department/ it will display total number of vacencies in particular departments.
