from django.conf.urls import include, url
from django.contrib import admin
from EmployeeManagement.views import EmpInfo , EmployeeOfDep ,NoVacancies ,EmpBYDesination

urlpatterns = [
    # Examples:
    # url(r'^$', 'EMS.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^employee_info/$', EmpInfo.as_view()),
    url(r'^employeebydepartment/(\d+)/$', EmployeeOfDep.as_view()),
    url(r'^no_of_vacancies_for_department/$', NoVacancies.as_view()),
    url(r'^employeebydesination/([\w ]+)/$' ,EmpBYDesination.as_view()),
]
